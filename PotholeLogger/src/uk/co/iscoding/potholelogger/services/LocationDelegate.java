package uk.co.iscoding.potholelogger.services;

import android.location.Location;

public interface LocationDelegate {

    public void locationChanged(Location location);
    
}
