package uk.co.iscoding.potholelogger.services;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationService implements LocationListener {

    private final static String PROVIDER = LocationManager.GPS_PROVIDER;
    
    private final LocationDelegate delegate;
    private final LocationManager locManager;
    private boolean isUpdating = false;
    
    public LocationService(LocationDelegate delegate, LocationManager locManager) {
        this.delegate = delegate;
        this.locManager = locManager;
    }
    
    public void startUpdating(int minInterval, int minDistance) {
        if (locManager.isProviderEnabled(PROVIDER) && !isUpdating) {
            locManager.requestLocationUpdates(PROVIDER, minInterval, minDistance, this);
            isUpdating = true;
        }
    }
    
    public void stopUpdating() {
        if (isUpdating) {
            locManager.removeUpdates(this);
            isUpdating = false;
        }
    }
    
    public Location getLastKnownLocation() {
        return locManager.getLastKnownLocation(PROVIDER);
    }

    @Override public void onLocationChanged(Location location) {
        delegate.locationChanged(location);
    }
    
    @Override public void onProviderDisabled(String provider) {}
    @Override public void onProviderEnabled(String provider) {}
    @Override public void onStatusChanged(String provider, int status, Bundle extras) {}

}
