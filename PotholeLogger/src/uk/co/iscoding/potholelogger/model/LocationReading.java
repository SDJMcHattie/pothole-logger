package uk.co.iscoding.potholelogger.model;

import android.location.Location;
import android.text.format.Time;

public class LocationReading {

    private double latitude;
    private double longitude;
    private float accuracy;
    private long time;
    
    public LocationReading(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.accuracy = location.getAccuracy();
        this.time = location.getTime();
    }
    
    public double getLat() {
        return latitude;
    }
    
    public double getLon() {
        return longitude;
    }
    
    public float getAccuracy() {
        return accuracy;
    }
    
    public long getTime() {
        return time;
    }
    
    public String getFormattedTime() {
        Time time = new Time();
        time.set(this.time);
        return time.format("%d/%m/%Y %H:%M:%S");
    }
    
}
