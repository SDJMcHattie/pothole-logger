package uk.co.iscoding.potholelogger.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import uk.co.iscoding.potholelogger.R;
import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.google.gson.Gson;

public class LocationRepository {
    
    private class JsonData {
        private final List<LocationReading> readings;
        
        private JsonData() {
            readings = Lists.newArrayList();
        }
    }
    
    private static String JSONFILENAME = "data.json";
    
    private enum MediaState {
        WRITABLE,
        READABLE,
        UNAVAILABLE,
    }
    
    private final Context ctx;
    private JsonData jsonData;
    
    public LocationRepository(Context ctx) {
        this.ctx = ctx;
        if (canReadExternalStorageFile()) {
            loadRepository();
        } else {
            Toast.makeText(ctx, R.string.noJsonDataFile, Toast.LENGTH_LONG).show();
            jsonData = new JsonData();
        }
    }
    
    private MediaState getMediaState() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            return MediaState.WRITABLE;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            return MediaState.READABLE;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            return MediaState.UNAVAILABLE;
        }
    }

    private File getJsonFile() {
        if (getMediaState() != MediaState.UNAVAILABLE) {
            File external = ctx.getExternalFilesDir(null);
            return new File(external, JSONFILENAME);
        } else {
            return null;
        }
    }
    
    private boolean canReadExternalStorageFile() {
        File file = getJsonFile();
        if (file != null) {
            return file.exists();
        }
        return false;
    }
    
    public int getCount() {
        return jsonData.readings.size();
    }
    
    public List<LocationReading> getReadings() {
        return Collections.unmodifiableList(jsonData.readings);
    }
    
    public void addLocation(Context ctx, Location location) {
        jsonData.readings.add(new LocationReading(location));
        saveRepository();
    }
    
    public void removeAllLocations() {
        jsonData.readings.clear();
        saveRepository();
    }
    
        
    public void loadRepository() {
        File file = getJsonFile();
        if (file != null) {
            Gson gson = new Gson();
            try {                
                BufferedReader br = new BufferedReader(new FileReader(file));         
                jsonData = gson.fromJson(br, JsonData.class);
            } catch (IOException e) {
                Toast.makeText(ctx, R.string.couldntLoadReadings, Toast.LENGTH_LONG).show();
            }
        }
    }
    
    public void saveRepository() {
        File file = getJsonFile();
        if (file != null) {
            Gson gson = new Gson();
            String json = gson.toJson(jsonData);
            try {
                FileWriter writer = new FileWriter(file);
                writer.write(json);
                writer.close();
            } catch (IOException e) {
                Toast.makeText(ctx, R.string.couldntSaveReadings, Toast.LENGTH_LONG).show();
            }
        }
    }
    
}
