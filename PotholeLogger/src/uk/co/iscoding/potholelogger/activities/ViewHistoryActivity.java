package uk.co.iscoding.potholelogger.activities;

import uk.co.iscoding.potholelogger.R;
import uk.co.iscoding.potholelogger.model.LocationReading;
import uk.co.iscoding.potholelogger.model.LocationRepository;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ViewHistoryActivity extends Activity {

    public static Intent createIntent(Context ctx) {
        return new Intent(ctx, ViewHistoryActivity.class);
    }
    
    private LocationRepository locationRepo;
    private TextView historicDataTextView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history);
        
        historicDataTextView = (TextView)findViewById(R.id.historicDataTextView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationRepo = new LocationRepository(this);
        historicDataTextView.setText(getFormattedData());
    }

    @Override
    protected void onPause() {
        locationRepo = null;
        super.onPause();
    }

    private String getFormattedData() {
        StringBuilder sb = new StringBuilder();
        
        int counter = 1;
        for (LocationReading reading : locationRepo.getReadings()) {
            if (sb.toString().length() > 0) {
                sb.append("\n\n----------\n\n");
            }
            sb.append(getString(R.string.formattedDataReading) + " " + counter++ + ":   " + reading.getFormattedTime() + "\n");
            sb.append(getString(R.string.formattedDataLatitude) + ": " + reading.getLat() + "\n");
            sb.append(getString(R.string.formattedDataLongitude) + ": " + reading.getLon() + "\n");
            sb.append(getString(R.string.formattedDataAccuracy) + ": " + reading.getAccuracy() + " " + getString(R.string.formattedDataAccuracyUnit));
        }
        
        return sb.toString();
    }
    
    public void clearHistory(View v) {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(R.string.confirmDeleteDialogTitle)
        .setMessage(R.string.confirmDeleteDialogMessage)
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Stop the activity
                locationRepo.removeAllLocations();
                historicDataTextView.setText(getFormattedData());
            }
        })
        .setNegativeButton(android.R.string.no, null)
        .show();

    }
    
}
