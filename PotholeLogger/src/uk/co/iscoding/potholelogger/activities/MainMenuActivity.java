package uk.co.iscoding.potholelogger.activities;

import uk.co.iscoding.potholelogger.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainMenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
	}

	public void displayTakeReadings(View v) {
	    startActivity(TakeReadingsActivity.createIntent(this));
	}
	
	public void displayHistoricalReadings(View v) {
	    startActivity(ViewHistoryActivity.createIntent(this));
	}
}
