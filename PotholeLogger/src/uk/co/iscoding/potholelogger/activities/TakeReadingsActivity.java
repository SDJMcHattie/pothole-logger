package uk.co.iscoding.potholelogger.activities;

import uk.co.iscoding.potholelogger.R;
import uk.co.iscoding.potholelogger.model.LocationRepository;
import uk.co.iscoding.potholelogger.services.LocationDelegate;
import uk.co.iscoding.potholelogger.services.LocationService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class TakeReadingsActivity extends Activity implements LocationDelegate {
    
    private final static int TIME_INTERVAL = 100;  // milliseconds
    private final static int DISTANCE_INTERVAL = 10;  // metres

    public static Intent createIntent(Context ctx) {
        return new Intent(ctx, TakeReadingsActivity.class);
    }
    
    private LocationService locationService;
    private LocationRepository locationRepo;
    private Button takeAReadingButton;
    private TextView buttonWillAppearTextView;
    private TextView numberOfReadingsTextView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_readings);

        // Stay awake during taking readings
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        locationService = new LocationService(this, (LocationManager) getSystemService(Context.LOCATION_SERVICE));
        takeAReadingButton = (Button)findViewById(R.id.takeAReadingButton);
        buttonWillAppearTextView = (TextView)findViewById(R.id.buttonWillAppearTextView);
        numberOfReadingsTextView = (TextView)findViewById(R.id.numberOfReadingsTextView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationRepo = new LocationRepository(this);
        numberOfReadingsTextView.setText("" + locationRepo.getCount());
        takeAReadingButton.setVisibility(View.INVISIBLE);
        buttonWillAppearTextView.setVisibility(View.VISIBLE);
        locationService.startUpdating(TIME_INTERVAL, DISTANCE_INTERVAL);
    }

    @Override
    protected void onPause() {
        locationService.stopUpdating();
        locationRepo = null;
        super.onPause();
    }

    public void takeAReading(View v) {
        locationRepo.addLocation(this, locationService.getLastKnownLocation());
        numberOfReadingsTextView.setText("" + locationRepo.getCount());
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {}
    }

    @Override
    public void locationChanged(Location location) {
        takeAReadingButton.setVisibility(View.VISIBLE);
        buttonWillAppearTextView.setVisibility(View.INVISIBLE);
    }
        
}
